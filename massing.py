class GateWarsMassingBot:

    def __init__(self, browser):
        from selenium import webdriver
        from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
        # first it waits for you to input what browser you want to use then sets a page load stratigy so that
        if browser == 'f':
            path = '/home/jacks/PycharmProjects/Grey/geckodriver'
            cap = DesiredCapabilities.FIREFOX.copy()
            cap["pageLoadStrategy"] = 'none'
            self.driver = webdriver.Firefox(executable_path=path, capabilities=cap)
            self.driver.get('https://main.gatewa.rs/')
        elif browser == 'c':
            path = '/home/jacks/PycharmProjects/Grey/chromedriver'
            cap = DesiredCapabilities.CHROME
            cap["pageLoadStrategy"] = 'none'
            self.driver = webdriver.Chrome(executable_path=path, desired_capabilities=cap)
        else:
            print('not option')

    def normal_attack(self, rperam, train_count, troop_count):
        from selenium.common.exceptions import NoSuchElementException
        import time
        att_count = 0
        r_count = 0
        deff_count = 0
        train_counter = 0
        ms_counter = 0
        input('press enter....')
        self.driver.switch_to_window(self.driver.window_handles[0])

        while att_count < 5:

            try:

                self.driver.switch_to_window(self.driver.window_handles[0])
                att_butt = self.driver.find_element_by_xpath('/html/body/div/div/main/div[1]/div[1]/form/div/span[2]/button')
                att_butt.click()
                self.driver.execute_script('window.stop();')
                att_count += 1
                r_count += 1
                deff_count += 1
                train_counter += 1
                ms_counter += 1

                # to stop blitz
                if att_count == 5:
                    time.sleep(1)
                    att_count -= att_count
                    continue

                if deff_count >= 20:
                    print('checking defense')
                    att_butt = self.driver.find_element_by_xpath('/html/body/div/div/main/div[1]/div[1]/form/div/span[2]/button')
                    att_butt.click()
                    time.sleep(3)
                    deff = self.driver.find_element_by_xpath('/html/body/div/div/main/div[1]/div/div[2]/div/div[2]/div[3]')
                    num = int(deff.text)
                    if num in range(0, 7):
                        deff_count -= deff_count
                        break
                    else:
                        self.driver.back()
                        deff_count -= deff_count
                        continue
                if r_count >= rperam:
                    self.driver.switch_to_window(self.driver.window_handles[1])
                    self.driver.implicitly_wait(3)
                    try:
                        self.driver.refresh()
                        self.driver.implicitly_wait(3)
                        r_naq = self.driver.find_element_by_css_selector(
                            'table.table-armory:nth-child(4) > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(3) > form:nth-child(1) > div:nth-child(3) > span:nth-child(3) > input:nth-child(1)')
                        pages_text = str(r_naq.get_attribute("outerHTML"))
                        r_naq_amou =''.join(pages_text[117:130].strip().split(','))
                        self.driver.switch_to_window(self.driver.window_handles[3])
                        self.driver.implicitly_wait(3)
                        naq_input = self.driver.find_element_by_xpath('//*[@id="withdraw_amount"]')
                        naq_input.send_keys(int(r_naq_amou))
                        withdraw = self.driver.find_element_by_xpath('/html/body/div/div/main/section/div[4]/form/button')
                        withdraw.click()
                        self.driver.switch_to_window(self.driver.window_handles[1])
                        self.driver.implicitly_wait(3)
                        self.driver.find_element_by_xpath('/html/body/div/div/main/table/tbody/tr[1]/td[3]/form/div/span[2]/input').click()
                        r_count -= r_count
                        checkmark = self.driver.find_element_by_xpath("//div[contains(@class,'recaptcha-checkbox-checkmark')]")
                        if checkmark is True:
                            checkmark.click()
                            self.driver.find_element_by_xpath("//input[contains(@class,'btn btn-lg btn-outline-secondary')]").click()
                            continue
                        else:
                            continue
                    except ValueError as e:
                        continue
                    except NoSuchElementException as e:
                            continue

                if train_count <= train_counter:
                    self.driver.switch_to_window(self.driver.window_handles[2])
                    super_troop_count = self.driver.find_element_by_xpath(
                        '/html/body/div/div/main/div[1]/div[2]/table/tbody/tr[3]/td[2]/button').text
                    troops_clean = ''.join(super_troop_count.strip().split(','))
                    if int(troops_clean) < troop_count:
                        for n in range(2000000, 6000000):
                            basic_unit = n * 2000
                            super_unit = n * 98000
                            naq = super_unit + basic_unit
                            self.driver.switch_to_window(self.driver.window_handles[3])
                            self.driver.implicitly_wait(3)
                            naq_input = self.driver.find_element_by_xpath('//*[@id="withdraw_amount"]')
                            naq_input.send_keys(int(naq))
                            withdraw = self.driver.find_element_by_xpath(
                                '/html/body/div/div/main/section/div[4]/form/button')
                            withdraw.click()
                            self.driver.switch_to_window(self.driver.window_handles[2])
                            self.driver.implicitly_wait(3)
                            normal_troop = self.driver.find_element_by_xpath('//*[@id="train_atsold"]')
                            super_troop = self.driver.find_element_by_xpath('//*[@id="train_satsold"]')
                            order_butt = self.driver.find_element_by_xpath('/html/body/div/div/main/div[2]/div[3]/form[1]/input[2]')
                            normal_troop.send_keys(str(basic_unit))
                            order_butt.click()
                            self.driver.implicitly_wait(3)
                            order_butt_2 = self.driver.find_element_by_xpath('/html/body/div/div/main/div[2]/div[3]/form[1]/input[2]')

                            super_troop.send_keys(str(super_unit))
                            order_butt_2.click()
                if ms_counter >= 1:
                    ms = self.driver.find_element_by_xpath('/html/body/div/div/main/div[1]/div/table[1]/tbody/tr[3]')
                    print(str(ms.get_attribute("outerHTML")))
                    print(str(ms.get_attribute('innerHTML')))
                    ms2 = self.driver.find_element_by_tag_name('td')
                    for el in ms2:
                        print(el.text)

            except NoSuchElementException as e:
                print(e)
                self.driver.switch_to_window(self.driver.window_handles[0])
                continue


bot = GateWarsMassingBot('f')
bot.normal_attack(15, 15, 800000000)
