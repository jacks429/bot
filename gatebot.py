import sys, os
from collections import OrderedDict


class GateWarsFarmBot:

    def __init__(self, browser):
        from selenium import webdriver
        from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

        if browser == 'f':
            path = '/home/jacks/PycharmProjects/Grey/geckodriver'
            cap = DesiredCapabilities.FIREFOX.copy()
            cap["pageLoadStrategy"] = 'none'
            self.driver = webdriver.Firefox(executable_path=path, capabilities=cap)

        elif browser == 'c':
            path = '/home/jacks/PycharmProjects/Grey/chromedriver'
            cap = DesiredCapabilities.CHROME
            cap["pageLoadStrategy"] = 'none'
            self.driver = webdriver.Chrome(executable_path=path, desired_capabilities=cap)
        else:
            print('not option')

        self.driver.get('https://main.gatewa.rs/')
        self.targets = OrderedDict()
        self.browser = str(browser)
        self.active = 1
        self.purge = False
        self.purge_pages = 0
        self.bank_per = 0
        self.farm = 0
        self.naq_total = 0

    def farmrepair(self):

        try:
            self.driver.refresh()

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)

    def nextpage(self):
        from selenium.common.exceptions import NoSuchElementException
        from selenium.webdriver.common.by import By
        from selenium.webdriver.support.ui import WebDriverWait
        from selenium.webdriver.support import expected_conditions as ec
        from selenium.webdriver.common.action_chains import ActionChains

        try:

            nextpage = self.driver.find_element_by_xpath("(//a[@class='page-link'][contains(.,'Next')])[2]")
            nextpage.click()
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
        except NoSuchElementException:
            wait = WebDriverWait(self.driver, 10)
            att_butt = wait.until(ec.visibility_of_element_located(
                (By.XPATH, "(//a[@class='page-link'][contains(.,'Next')])[2]")))
            ActionChains(self.driver).move_to_element(att_butt).perform()
            self.driver.find_element_by_xpath("(//a[@class='page-link'][contains(.,'Next')])[2]").click()

    def maketargetlist(self):
        from selenium.common.exceptions import StaleElementReferenceException

        try:

            print('creating target list...')
            attackbuttons = self.driver.find_elements_by_xpath("//*[contains(@class, 'btn btn-sm btn-click btn-outline-secondary hidden-lg-up')]//i[contains(@class, 'fa fa-crosshairs')]")
            funds = self.driver.find_elements_by_id('naq')
            
            clean_funds1 = [fund.text.split('Naquadah')[0] for fund in funds]
            clean_funds2 = [''.join(fund.strip().split(',')) for fund in clean_funds1]
            clean_funds3 = [''.join(fund[::-1]) for fund in clean_funds2]

            self.targets = {fund: button for fund, button in zip(clean_funds3, attackbuttons)}
            self.targets = {k: v for k, v in self.targets.items() if k != '?????'}
            self.targets = {int(k): v for k, v in self.targets.items()}

            self.targets = {k: v for k, v in self.targets.items() if k >= self.farm}
        except KeyError:
            pass
        except StaleElementReferenceException:
            self.driver.refresh()
            self.targets.clear()
            self.maketargetlist()

    def get_page_numbers(self):

        if self.purge == True:
            pass

        else:
            pages = self.driver.find_element_by_xpath("//html/body/div/div/main/div[1]/div/form/div/nav/ul/li[2]/span")
            pages_text = str(pages.get_attribute('textContent')).strip()
            x, first, second = [int(s) for s in pages_text.split() if s.isdigit()]
            return second

    def farmperamattack(self):
        import time
        from selenium.webdriver.common.by import By
        from selenium.webdriver.support.ui import WebDriverWait
        from selenium.webdriver.support import expected_conditions as ec
        from selenium.webdriver.common.action_chains import ActionChains
        from  selenium.common.exceptions import StaleElementReferenceException

        print('attacking...')
        second = self.get_page_numbers()
        pages = second
        pages_done = 0
        pages += 1
        while pages_done < pages:
            if not bool(self.targets):
                self.nextpage()
                time.sleep(1)
                pages_done += 1
                self.maketargetlist()
                continue

            try:
                for k, v in self.targets.items():

                    """ wait = WebDriverWait(self.driver, 10)
                    att_butt = wait.until(ec.visibility_of_element_located((By.XPATH, "//*[contains(@class, 'btn btn-sm btn-click btn-outline-secondary hidden-lg-up")))
                    ActionChains(self.driver).move_to_element(att_butt).perform()"""
                   self.driver.execute_script("arguments[0].click();", v)
                   self.driver.find_element_by_xpath("/html/body/div/div/main/table/tbody/tr[1]/td[3]/form/div/span[2]/input").click()
                   time.sleep(0.2)
                   self.driver.execute_script('window.stop();')
                   self.targets = {fund: button for fund, button in self.targets.items() if fund != k}
                   self.naq_total += k
                   if not bool(self.targets):
                        self.nextpage()
                        time.sleep(1)
                        pages_done += 1
                        self.maketargetlist()
                        continue
                if self.naq_total >= self.bank_per:
                        self.farmrepair()
                        self.driver.execute_script('window.stop();')
                        self.driver.switch_to_window(self.driver.window_handles[-1])
                        self.driver.refresh()
                        input('press enter to cotinue...')
                        time.sleep(0.3)
                        self.naq_total -= self.naq_total
                        self.driver.switch_to.window(self.driver.window_handles[0])
                        self.targets.clear()
                        self.maketargetlist()
                        continue

            except StaleElementReferenceException as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                self.targets.clear()
                self.maketargetlist()
                continue


    def purge_attack(self):
        import time
        from selenium.webdriver.common.by import By
        from selenium.webdriver.support.ui import WebDriverWait
        from selenium.webdriver.support import expected_conditions as ec
        from selenium.webdriver.common.action_chains import ActionChains
        from  selenium.common.exceptions import NoSuchElementException
        from  selenium.common.exceptions import StaleElementReferenceException

        print('attacking...')
        second = self.get_page_numbers()
        pages_done = 0
        att_count = 0
        while self.active is 1:
            if not bool(self.targets):
                self.nextpage()
                time.sleep(1)
                pages_done += 1
                self.maketargetlist()
                continue

            try:
                for k, v in self.targets.items():



                   """ wait = WebDriverWait(self.driver, 10)
                    att_butt = wait.until(ec.visibility_of_element_located((By.XPATH, "//*[contains(@class, 'btn btn-sm btn-click btn-outline-secondary hidden-lg-up")))
                    ActionChains(self.driver).move_to_element(att_butt).perform()"""
                   time.sleep(0.1)
                   self.driver.execute_script("arguments[0].click();", v)
                   att_count += 1
                   time.sleep(0.2)
                   self.driver.execute_script('window.stop();')
                   self.targets = {fund: button for fund, button in self.targets.items() if fund != k}
                   self.naq_total += k
                   if not bool(self.targets):
                        self.nextpage()
                        time.sleep(1)
                        pages_done += 1
                        self.maketargetlist()
                        continue
                   if att_count == 30:
                       self.driver.find_element_by_xpath("//input[contains(@name,'doallattrepair6')]")
                       att_count -= att_count
                       continue

            except StaleElementReferenceException as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                print(exc_type, fname, exc_tb.tb_lineno)
                self.targets.clear()
                self.maketargetlist()
                continue
            except NoSuchElementException as e:
                self.driver.refresh()
                time.sleep(0.3)
                self.targets.clear()
                self.maketargetlist()
                continue

def main():

        browser = input('Select browser f(Firefox) c(Chrome)...')
        bot = GateWarsFarmBot(browser)
        while bot.active == 1:
            input("Browser set press Enter to continue...")
            purge = input('Purge... y or n')


            bot.farm = int(input('Enter farm peram'))
            bot.bank_per = int(input('Enter bank peram'))
            input('Press Enter to start attack as long as your on war page and tabs are set (War, Armory and Bank)...')
            if purge == 'y':
                bot.purge = True
                bot.maketargetlist()
                bot.purge_attack()
                continue
            else:
                bot.maketargetlist()
                bot.farmperamattack()
                continue




if __name__ == '__main__':
    main()
